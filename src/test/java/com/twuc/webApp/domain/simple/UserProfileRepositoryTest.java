package com.twuc.webApp.domain.simple;

import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.domain.ValueInClosure;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class UserProfileRepositoryTest extends JpaTestBase {

    @Autowired
    private UserProfileRepository repository;

    @Test
    void should_save_and_get_entity() {
        // 我用随机的数值初始化，这样就没有通过写死的办法来过测试了，哼哼哼哼~
        final String expectedName = UUID.randomUUID().toString();
        final short expectedYearOfBirth = (short)(1900 + new Random().nextInt(100));
        final ValueInClosure<Long> expectedId = new ValueInClosure<>();

        flushAndClear(em -> {
            final UserProfile entity = UserProfile.of(expectedName, expectedYearOfBirth);
            final UserProfile savedEntity = repository.save(entity);
            expectedId.setValue(savedEntity.getId());
        });

        run(em -> {
            final Optional<UserProfile> optional = repository.findById(expectedId.getValue());
            assertTrue(optional.isPresent());
            final UserProfile entity = optional.get();
            assertEquals(expectedId.getValue(), entity.getId());
            assertEquals(expectedName, entity.getName());
            assertEquals(expectedYearOfBirth, entity.getYearOfBirth().shortValue());
        });
    }

    @Test
    void should_fail_if_user_name_is_null() {
        run(em -> {
            final UserProfile userProfile = UserProfile.of(null, (short)1990);
            assertThrows(DataIntegrityViolationException.class, () -> repository.save(userProfile));
        });
    }

    @Test
    @Transactional
    void should_delete_user() {
        final ValueInClosure<Long> userProfileId = new ValueInClosure<>();

        flushAndClear(em -> {
            final UserProfile userProfile = repository.save(UserProfile.of("Conan", (short) 1995));
            userProfileId.setValue(userProfile.getId());
        });

        clear(em -> {
            // TODO:
            //
            // 请书写程序删除刚刚保存的用户名为 "conan" 的 User Profile
            //
            // <--start-
            repository.deleteById(userProfileId.getValue());
            repository.flush();
            // --end-->
        });

        run(em -> {
            final Optional<UserProfile> userProfile = repository.findById(userProfileId.getValue());
            assertFalse(userProfile.isPresent());
        });
    }

    @Test
    void should_update_user_name() {
        final ValueInClosure<Long> userProfileId = new ValueInClosure<>();

        flushAndClear(em -> {
            final UserProfile userProfile = repository.save(UserProfile.of("Conan", (short) 1995));
            userProfileId.setValue(userProfile.getId());
        });

        clear(em -> {
            // TODO:
            //
            // 请书写程序将 "Conan" 用户的 User Profile 的用户名更新为 "Updated"
            //
            // <--start-
            UserProfile userProfile = em.find(UserProfile.class, userProfileId.getValue());
            userProfile.setName("Updated");
            em.merge(userProfile);
            em.flush();

            // --end-->
        });

        run(em -> {
            final UserProfile userProfile = repository
                .findById(userProfileId.getValue())
                .orElseThrow(RuntimeException::new);
            assertEquals("Updated", userProfile.getName());
        });
    }
}
