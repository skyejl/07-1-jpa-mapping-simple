package com.twuc.webApp.domain.simple;

import javax.persistence.*;

// TODO:
//
// 请创建一个 UserProfile entity。其对应的数据库表的 schema 必须满足如下 SQL 的要求：
//
// table: user_profile
// +────────────────+──────────────+───────────────────────────────+
// | column         | description  | additional                    |
// +────────────────+──────────────+───────────────────────────────+
// | id             | bigint       | auto_increment, primary key   |
// | name           | varchar(64)  | not null                      |
// | year_of_birth  | smallint     | not null                      |
// +────────────────+──────────────+───────────────────────────────+
//
//  请补全如下的代码，你可以新建各种方法、构造器，添加各种 annotation。但是添加的东西越少越好。
//
// <--start-
@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 64)
    private String name;
    @Column(nullable = false)
    private Short yearOfBirth;
    public static UserProfile of(String name, short yearOfBirth) {
        return new UserProfile(name, yearOfBirth);
    }

    private UserProfile(String name, Short yearOfBirth) {
        // 请实现该构造函数
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public UserProfile() {
    }

    public Long getId() {
        // 请实现该方法
        return this.id;
    }

    public String getName() {
        // 请实现该方法
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getYearOfBirth() {
        // 请实现该方法
        return this.yearOfBirth;
    }
}
// --end-->
